import React, { createContext, useContext, useState } from "react";

const UserContext = createContext();
const UserDataProvider = ({ children }) => {
  const [userData, setUserData] = useState({});
  const [postComment, setPostComment] = useState();
  const [comment, setComment] = useState([]);
  const [posts, getPosts] = useState();
  const [selectedPost, setSelectedPost] = useState();
  return (
    <UserContext.Provider
      value={{
        posts,
        getPosts,
        userData,
        setUserData,
        postComment,
        setPostComment,
        comment,
        setComment,
        selectedPost,
        setSelectedPost,
      }}
    >
      {children}
    </UserContext.Provider>
  );
};
function useUserData() {
  return useContext(UserContext);
}
export { useUserData, UserDataProvider };
