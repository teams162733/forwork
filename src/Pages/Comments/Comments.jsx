import React, { Suspense, useEffect, useState } from "react";
import { useUserData } from "../../Contex/userContex";
import { ToastContainer, toast } from "react-toastify";
import Loading from "../../Layout/Loading";
const CommentsList = React.lazy(() => import("./CommentsList"));

const Comments = () => {
  const {
    postComment,
    setComment,
    setPostComment,
    comment,
    userData,
    selectedPost,
  } = useUserData();
  const [input, setInput] = useState("");
  const addComment = () => {
    const commentData = {
      postId: selectedPost.id,
      userId: userData.id,
      id: comment[comment.length - 1].id + 1,
      email: userData.email,
      body: input,
    };
    fetch(`http://localhost:8000/comments`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(commentData),
    });
    fetch("http://localhost:8000/comments")
      .then((res) => res.json())
      .then((data) => {
        let commentsPost = data?.filter(
          (c) => selectedPost.userId == c.userId && c.postId == selectedPost.id
        );
        setPostComment(commentsPost);
        setComment(data);
        toast.success("Comment added");
      })
      .catch((error) => {
        toast.warning("Xatolik yuz berdi:", error.message);
      });
    setInput("");
  };
  useEffect(() => {}, [selectedPost]);
  return (
    <>
      <div className="comments-page">
        <h1 className="sec-name">Comments</h1>
        <Suspense fallback={<Loading />}>
          <CommentsList postComment={postComment} />
        </Suspense>
        {postComment ? (
          <div className="add-comment">
            <input
              type="text"
              value={input}
              placeholder="Add a new task"
              onChange={(e) => setInput(e.target.value)}
            />
            <button onClick={addComment}>Add</button>
          </div>
        ) : (
          <></>
        )}
      </div>
      <ToastContainer />
    </>
  );
};

export default Comments;
