import React, { useState } from "react";
import ModalEdit from "../../components/Modal/modalEdit";
import "../../styles/App.css";
import ModalDelete from "../../components/Modal/modalDelete";

const CommentsList = ({ postComment }) => {
  const [isDelete, setIsDelete] = useState(false);
  const [isEdit, setIsEdit] = useState(false);
  const [currentItem, setCurrentItem] = useState();
  const [updateComments, setUpdateComments] = useState("");
  const [id, setId] = useState();
  const openEdit = (item) => {
    setIsEdit(true);
    setCurrentItem(item);
    setUpdateComments(item.body);
    setId(item.id);
  };
  const openDelete = (item) => {
    setIsDelete(true);
    setId(item.id);
  };
  return (
    <>
      {isEdit && (
        <ModalEdit
          setUpdateComments={setUpdateComments}
          updateComments={updateComments}
          isEdit={isEdit}
          setIsEdit={setIsEdit}
          currentItem={currentItem}
          id={id}
          setId={setId}
        />
      )}
      {isDelete && (
        <ModalDelete setIsDelete={setIsDelete} id={id} setId={setId} />
      )}
      <div className="comments-block">
        <h1></h1>
        {postComment && postComment ? (
          postComment.map((item, index) => (
            <div className="comment" key={index}>
              <div className="comment-author">
                <h5>{item.email}</h5>
              </div>
              <p>{item.body}</p>
              <div className="comment-bottom">
                <div className="comment-buttons">
                  <button
                    className="button-edit"
                    onClick={() => openEdit(item)}
                  >
                    <i className="fa fa-edit"></i>
                  </button>
                  <button
                    className="button-delete"
                    onClick={() => openDelete(item)}
                  >
                    <i className="fa fa-trash"></i>
                  </button>
                </div>
              </div>
            </div>
          ))
        ) : (
          <div className="select-posts">Select a post</div>
        )}
      </div>
    </>
  );
};
export default CommentsList;
