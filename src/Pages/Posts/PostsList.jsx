import React, { useState } from "react";
import PostDelete from "../../components/Modal/postDelete";

const PostsList = ({ filteredPosts, getComments, startEdit }) => {
  const [isDelete, setIsDelete] = useState(false);
  const [id, setId] = useState();
  const startDelete = (item) => {
    setIsDelete(true);
    setId(item.id);
  };
  return (
    <>
      {isDelete && (
        <PostDelete setIsDelete={setIsDelete} id={id} setId={setId} />
      )}
      <div className="posts-name">
        {filteredPosts &&
          filteredPosts?.map((item, index) => (
            <li
              className="list-item"
              key={index}
              onClick={() => getComments(item)}
            >
              <button type="button" className="list">
                {item.title}
              </button>
              <div className="posts-crud-buttons">
                <button className="button-edit" onClick={() => startEdit(item)}>
                  <i className="fa fa-edit"></i>
                </button>
                <button
                  className="button-delete"
                  onClick={() => startDelete(item)}
                >
                  <i className="fa fa-trash"></i>
                </button>
              </div>
            </li>
          ))}
      </div>
    </>
  );
};

export default PostsList;
