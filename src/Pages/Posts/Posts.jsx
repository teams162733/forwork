import React, { Suspense, useEffect, useState } from "react";
import { useUserData } from "../../Contex/userContex";
import { ToastContainer, toast } from "react-toastify";
import Loading from "../../Layout/Loading";
const PostsList = React.lazy(() => import("./PostsList"));
const Posts = () => {
  const [openAdd, setOpenInput] = useState(false);
  const [openEdit, setOpenEdit] = useState(false);
  const [addInput, setAddInput] = useState("");
  const [updatePosts, setUpdatePosts] = useState("");
  const [filteredPosts, setFilteredPosts] = useState();
  const [editId, setEditId] = useState();
  const user = JSON.parse(localStorage.getItem("userData"));
  const {
    getPosts,
    userData,
    setPostComment,
    posts,
    comment,
    setSelectedPost,
  } = useUserData();
  useEffect(() => {
    let userPosts = posts?.filter((item) => item.userId == user.id);
    setFilteredPosts(userPosts);
  }, [posts]);
  const getComments = (item) => {
    setSelectedPost(item);
    let commentsPost = comment?.filter(
      (c) => item.userId == c.userId && c.postId == item.id
    );
    setPostComment(commentsPost);
  };
  const seeAdd = () => {
    if (!openAdd) setOpenInput(true);
    else setOpenInput(false);
  };
  const validate = () => {
    var result = true;
    if (addInput === "" || addInput === null) {
      result = false;
      toast.warning("Please Enter Name Post");
    }
    return result;
  };
  const addPosts = () => {
    if (validate()) {
      const data = {
        userId: userData.id,
        id: posts[posts.length - 1]?.id + 1 || 1,
        title: addInput,
      };
      fetch(`http://localhost:8000/posts`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
      })
        .then((res) => res.json())
        .then((data) => {
          let updatePosts = [...posts, data];
          getPosts(updatePosts);
          setAddInput("");
          toast.success("New post added");
        })
        .catch((error) => {
          toast.warning("Xatolik yuz berdi:", error);
        });
    }
  };
  const startEdit = (item) => {
    setEditId(item.id);
    setUpdatePosts(item.title);
    if (!openEdit) setOpenEdit(true);
    else setOpenEdit(false);
  };
  const updatePost = () => {
    if (updatePosts != "") {
      const updatedData = {
        userId: userData.id,
        id: editId,
        title: updatePosts,
      };
      fetch(`http://localhost:8000/posts/${editId}`, {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(updatedData),
      });
      fetch("http://localhost:8000/posts")
        .then((res) => res.json())
        .then((data) => {
          getPosts(data);
          setEditId(null);
          toast.success("Post edited");
        })
        .catch((error) => {
          toast.warning("Xatolik yuz berdi:", error);
        });
      setOpenEdit(false);
    } else {
      toast.warning("Please enter post");
    }
  };
  useEffect(() => {}, [posts]);
  return (
    <>
      <div className="posts-page">
        <ToastContainer />
        <h1 className="sec-name">{userData.userName} Posts</h1>
        <div className="posts-body">
          <div className="posts-buttons">
            <button className="button-add" onClick={seeAdd}>
              <i className="fa fa-plus"></i>
            </button>
          </div>
          {openAdd ? (
            <div className="crudPosts">
              <input
                type="text"
                value={addInput}
                onChange={(e) => setAddInput(e.target.value)}
              />
              <button onClick={addPosts} className="posts-crud-button">
                Add
              </button>
            </div>
          ) : (
            <></>
          )}
          {openEdit ? (
            <div className="crudPosts">
              <input
                type="text"
                value={updatePosts}
                onChange={(e) => setUpdatePosts(e.target.value)}
              />
              <button onClick={updatePost} className="posts-crud-button">
                OK
              </button>
            </div>
          ) : (
            <></>
          )}
          <Suspense fallback={<Loading />}>
            <PostsList
              filteredPosts={filteredPosts}
              getComments={getComments}
              startEdit={startEdit}
            />
          </Suspense>
        </div>
      </div>
    </>
  );
};

export default Posts;
