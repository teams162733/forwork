import React, { useEffect, useState } from "react";
import "./login.css";
import { useNavigate } from "react-router-dom";
import { ToastContainer, toast } from "react-toastify";
import { useUserData } from "../../Contex/userContex";
const Login = () => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const { setUserData, userData } = useUserData();
  const navigation = useNavigate();
  useEffect(() => {
    localStorage.clear();
  }, [userData]);
  const validate = () => {
    var result = true;
    if (username === "" || username === null) {
      result = false;
      toast.warning("Please Enter Name");
    }
    if (password === "" || password === null) {
      result = false;
      toast.warning("Please Enter Password");
    }
    return result;
  };
  async function submitUser(e) {
    e.preventDefault();
    if (validate()) {
      await fetch("http://localhost:9000/users")
        .then((res) => {
          return res.json();
        })
        .then((data) => {
          let isInUser = false;
          for (let i = 0; i < data.length; i++) {
            const element = data[i];
            if (
              element.username === username &&
              element.password === password
            ) {
              localStorage.setItem("userData", JSON.stringify(element));
              setUserData(element);
              isInUser = true;
              navigation("/main");
            }
          }
          if (!isInUser) {
            toast.warning("Bunday user mavjud emas");
          }
        })
        .catch((err) => {
          toast.error("Login Failed:" + err.message);
        });
    }
  }
  return (
    <>
      <ToastContainer />
      <div className="login-page">
        <form onSubmit={submitUser}>
          <h3>Login Here</h3>
          <label htmlFor="username">Username</label>
          <input
            type="text"
            placeholder="Email"
            id="username"
            onChange={(e) => setUsername(e.target.value)}
          />
          <label htmlFor="password">Password</label>
          <input
            type="password"
            placeholder="Password"
            id="password"
            onChange={(e) => setPassword(e.target.value)}
          />
          <button
            type="submit"
            className="button-login"
            style={{ marginTop: "24px" }}
          >
            Log In
          </button>
        </form>
      </div>
    </>
  );
};

export default Login;
