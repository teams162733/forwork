import "./styles/App.css";
import Categories from "./Layout/Categories";
import "react-toastify/dist/ReactToastify.css";
function App() {
  return (
    <div>
      <Categories />
    </div>
  );
}
export default App;
