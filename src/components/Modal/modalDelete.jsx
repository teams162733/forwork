import React, { useEffect } from "react";
import "./modal.css";
import { ToastContainer, toast } from "react-toastify";
import { useUserData } from "../../Contex/userContex";

const ModalDelete = ({ setIsDelete, id, setId }) => {
  const { comment, setComment, setPostComment, selectedPost } = useUserData();
  const toggleModal = () => {
    setIsDelete(false);
  };
  const startDelete = async () => {
    if (id) {
      try {
        const response = await fetch(`http://localhost:8000/comments/${id}`, {
          method: "DELETE",
          headers: {
            "Content-Type": "application/json",
          },
        });
        if (!response.ok) {
          throw new Error("Error occurred while deleting the comment.");
        }
        fetch(`http://localhost:8000/comments`)
          .then((res) => res.json())
          .then((data) => {
            let commentsPost = data?.filter(
              (c) =>
                selectedPost.userId == c.userId && c.postId == selectedPost.id
            );
            setPostComment(commentsPost);
            setComment(data);
          });
        toast.success("Comment deleted successfully.");
      } catch (error) {
        toast.error("Failed to delete the comment.");
      } finally {
        setIsDelete(false);
        setId(null);
      }
    }
  };
  useEffect(() => {}, [comment]);
  return (
    <>
      <ToastContainer />
      <div className="modal">
        <div class="overlay"></div>
        <div className="modal-content">
          <h5>Are you sure you want to delete this comment?</h5>
          <div className="modal-buttons">
            <button onClick={toggleModal}>CLOSE</button>
            <button onClick={startDelete}>DELETE</button>
          </div>
        </div>
      </div>
    </>
  );
};

export default ModalDelete;
