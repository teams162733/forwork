import React, { useEffect, useState } from "react";
import "./modal.css";
import { ToastContainer, toast } from "react-toastify";
import { useUserData } from "../../Contex/userContex";
const ModalEdit = ({
  isEdit,
  setIsEdit,
  currentItem,
  updateComments,
  setUpdateComments,
  id,
  setId,
}) => {
  const { comment, setComment, selectedPost, setPostComment } = useUserData();
  const toggleModal = () => {
    setIsEdit(false);
  };
  const validate = () => {
    var isValid = true;
    if (updateComments === "" || updateComments == null) {
      isValid = false;
      toast.warning("Please enter a comment");
    }
    return isValid;
  };
  const updateComment = () => {
    if (validate()) {
      try {
        const updatedData = {
          postId: currentItem.postId,
          userId: currentItem.userId,
          id: currentItem.id,
          email: currentItem.email,
          body: updateComments,
        };
        fetch(`http://localhost:8000/comments/${id}`, {
          method: "PUT",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify(updatedData),
        });
        fetch(`http://localhost:8000/comments`)
          .then((res) => res.json())
          .then((data) => {
            let commentsPost = data?.filter(
              (c) =>
                selectedPost.userId == c.userId && c.postId == selectedPost.id
            );
            setPostComment(commentsPost);
            setComment(data);
            toast.success("Comment updated successfully.");
          });
      } catch (error) {
        toast.error("Failed to update the comment.");
      } finally {
        setIsEdit(false);
        setId(null);
        setUpdateComments("");
      }
    }
  };
  useEffect(() => {}, [comment]);
  return (
    <>
      <ToastContainer />
      {isEdit && (
        <div className="modal">
          <div className="overlay" onClick={toggleModal}></div>
          <div className="modal-content">
            <h5>{currentItem.email}</h5>
            <textarea
              value={updateComments}
              onChange={(e) => setUpdateComments(e.target.value)}
            ></textarea>
            <div className="modal-buttons">
              <button onClick={toggleModal}>CLOSE</button>
              <button
                onClick={updateComment}
                style={{ backgroundColor: "green" }}
              >
                OK
              </button>
            </div>
          </div>
        </div>
      )}
    </>
  );
};
export default ModalEdit;
