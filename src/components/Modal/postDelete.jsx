import React, { useEffect } from "react";
import "./modal.css";
import { ToastContainer, toast } from "react-toastify";
import { useUserData } from "../../Contex/userContex";

const PostDelete = ({ setIsDelete, id, setId }) => {
  const { posts, getPosts } = useUserData();
  const toggleModal = () => {
    setIsDelete(false);
  };
  const startDelete = async () => {
    if (id) {
      try {
        const response = await fetch(`http://localhost:8000/posts/${id}`, {
          method: "DELETE",
          headers: {
            "Content-Type": "application/json",
          },
        });
        if (!response.ok) {
          throw new Error("Error occurred while deleting the post.");
        }
        fetch(`http://localhost:8000/posts`)
          .then((res) => res.json())
          .then((data) => {
            getPosts(data);
          });
        toast.success("Comment deleted successfully.");
      } catch (error) {
        toast.error("Failed to delete the comment.");
      } finally {
        setIsDelete(false);
        setId(null);
      }
    }
  };
  useEffect(() => {}, [posts]);
  return (
    <>
      <ToastContainer />
      <div className="modal">
        <div className="overlay"></div>
        <div className="modal-content">
          <h5>Are you sure you want to delete this comment?</h5>
          <div className="modal-buttons">
            <button onClick={toggleModal}>CLOSE</button>
            <button onClick={startDelete}>DELETE</button>
          </div>
        </div>
      </div>
    </>
  );
};
export default PostDelete;
