import React from "react";
import { useNavigate } from "react-router-dom";

const Navbar = () => {
  const navigation = useNavigate();
  const userData2 = JSON.parse(localStorage.getItem("userData"));
  const clearLogin = () => {
    localStorage.clear();
    navigation("/");
  };
  return (
    <>
      <nav className="navbar">
        <p className="header-logo">WorkTask</p>
        {userData2 ? (
          <button className="button-login" onClick={clearLogin}>
            Log out
          </button>
        ) : (
          <div></div>
        )}
      </nav>
    </>
  );
};

export default Navbar;
