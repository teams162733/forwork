import React, { useEffect, Suspense, lazy } from "react";
import { useNavigate } from "react-router-dom";
import { useUserData } from "../Contex/userContex";
import Loading from "./Loading";
const Posts = React.lazy(() => import("../Pages/Posts/Posts"));
const Comments = React.lazy(() => import("../Pages/Comments/Comments"));

const Main = () => {
  const { getPosts, setComment } = useUserData();
  useEffect(() => {
    fetch("http://localhost:8000/comments")
      .then((res) => res.json())
      .then((data) => setComment(data));
  }, []);
  useEffect(() => {
    fetch("http://localhost:8000/posts")
      .then((res) => res.json())
      .then((data) => getPosts(data));
  }, []);
  const navigate = useNavigate();
  useEffect(() => {
    const user = JSON.parse(localStorage.getItem("userData"));
    if (user == null) {
      navigate("/");
    }
  }, [navigate]);
  return (
    <div className="main-page">
      <div className="main-page-in">
        <Suspense fallback={<Loading />}>
          <Posts />
        </Suspense>
        <Suspense fallback={<Loading />}>
          <Comments />
        </Suspense>
      </div>
    </div>
  );
};

export default Main;
