import React from "react";
import { Outlet } from "react-router-dom";
import Navbar from "./navbar";

const RootLayout = () => {
  return (
    <div className="body-components">
      <div className="container">
        <header>
          <div>
            <Navbar />
          </div>
        </header>
        <main className="root-layout-main">
          <Outlet />
        </main>
      </div>
    </div>
  );
};

export default RootLayout;
