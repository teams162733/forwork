import React from "react";
import "../styles/App.css";
const Loading = () => {
  return <span className="loading">Loading . . .</span>;
};

export default Loading;
