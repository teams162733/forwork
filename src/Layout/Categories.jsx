import React from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import RootLayout from "./RootLayout";
import { UserDataProvider } from "../Contex/userContex";
import Main from "./Main";
import Login from "../Pages/Login/Login";

function Categories() {
  return (
    <div className="routers-body">
      <BrowserRouter>
        <UserDataProvider>
          <Routes>
            <>
              <Route path="/" element={<RootLayout />}>
                <Route index element={<Login />} />
                <Route path="main" element={<Main />} />
              </Route>
            </>
          </Routes>
        </UserDataProvider>
      </BrowserRouter>
    </div>
  );
}
export default Categories;
